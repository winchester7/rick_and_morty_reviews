<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Review;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $review1 = new Review();
        $review1
            ->setEpisodeId(3)
            ->setText('Test text with sentiment -0.9')
            ->setSentiment(-0.9);
        $manager->persist($review1);

        $review2 = new Review();
        $review2
            ->setEpisodeId(3)
            ->setText('Test text with sentiment -0.2')
            ->setSentiment(-0.2);
        $manager->persist($review2);

        $manager->flush();
    }
}
