<?php

namespace App\Controller;

use App\Repository\ReviewRepository;
use Symfony\Component\HttpFoundation\{
    Request,
    Response,
    JsonResponse
};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use NickBeen\RickAndMortyPhpApi\Episode;
use NickBeen\RickAndMortyPhpApi\Exceptions\NotFoundException;
use Sentiment\Analyzer;
use App\Entity\Review;
use Symfony\Component\Routing\Exception\InvalidArgumentException;

class EpisodesController extends AbstractController
{
    #[Route('/episodes')]
    public function getList(Request $request): Response
    {
        try {
            $page = intval($request->query->get('page')) ?: 1;

            $content = [];

            $episodes = new Episode();
            $episodesCollection = $episodes->page($page)->get();

            foreach ($episodesCollection->results as $episode) {
                $content[] = $this->getEpisodeArr($episode);
            }

            return new JsonResponse(
                [
                    'status' => 'ok',
                    'data' => $content
                ],
                Response::HTTP_OK
            );

        } catch (NotFoundException $e) {

            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage()]
                ],
                Response::HTTP_NOT_FOUND
            );

        } catch (\Throwable $e) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage(),]
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    #[Route('/episodes/{code}', methods: ['GET'])]
    public function getEpisode(ReviewRepository $reviewRepository, Request $request, string $code): Response
    {
        try {
            $episodes = new Episode();
            $episodesCollection = $episodes->withEpisode($code)->get();

            $episode = [];
            foreach ($episodesCollection->results as $res) {
                $episode = $this->getEpisodeArr($res);
            }

            $episode['reviews'] = [];
            $reviews = $reviewRepository->findBy(
                ['episodeId' => $episode['id']],
                ['sentiment' => 'DESC'],
                3
            );

            foreach($reviews as $review) {
                $episode['reviews'][] = $this->getReviewArr($review);
            }

            return new JsonResponse(
                [
                    'status' => 'ok',
                    'data' => $episode
                ],
                Response::HTTP_OK
            );

        } catch (NotFoundException $e) {

            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage()]
                ],
                Response::HTTP_NOT_FOUND
            );

        } catch (\Throwable $e) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage(),]
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    #[Route('/episodes/{code}/review', methods: ['POST'])]
    public function postEpisodeReview(ManagerRegistry $doctrine, Request $request, string $code): Response
    {
        try {
            $episodes = new Episode();
            $episodesCollection = $episodes->withEpisode($code)->get();

            foreach ($episodesCollection->results as $res) {
                $episode = $this->getEpisodeArr($res);
            }

            $text = $request->request->get('text');
            if (empty($text))
                throw new InvalidArgumentException("You have to write a review text.");

            $analyzer = new Analyzer();
            $sentiment = $analyzer->getSentiment($text)['compound'];

            $entityManager = $doctrine->getManager();

            $review = new Review();
            $review
                ->setText($text)
                ->setSentiment($sentiment)
                ->setEpisodeId($episode['id']);

            $entityManager->persist($review);
            $entityManager->flush();

            return new JsonResponse(
                [
                    'status' => 'ok',
                    'data' => [
                        'message' => "Your review was successfully added with id {$review->getId()} and sentiment {$sentiment}."
                    ]
                ],
                Response::HTTP_OK
            );

        } catch (NotFoundException $e) {

            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage()]
                ],
                Response::HTTP_NOT_FOUND
            );

        } catch (InvalidArgumentException $e) {

            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage()]
                ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (\Throwable $e) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'data' => ['message' => $e->getMessage(),]
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    private function getEpisodeArr(\NickBeen\RickAndMortyPhpApi\Dto\Episode $episode) {
        return [
            'id' => $episode->id,
            'name' => $episode->name,
            'code' => $episode->episode,
            'release' => $episode->air_date,
        ];
    }

    private function getReviewArr(Review $review) {
        return [
            'text' => $review->getText(),
            'sentiment' => $review->getSentiment(),
        ];
    }
}