<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class EpisodesControllerTest extends WebTestCase
{
    public function testEpisodesTooBigPage()
    {
        $client = static::createClient();
        $client->request('GET', '/episodes?page=99999');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testEpisodesIsOk()
    {
        $client = static::createClient();
        $client->request('GET', '/episodes');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('ok', $responseData['status']);

        $episode = array_shift($responseData['data']);
        $this->assertSame(true, isset($episode['id']));
        $this->assertSame(true, is_int($episode['id']));
        $this->assertSame(true, !empty($episode['name']));
        $this->assertSame(true, !empty($episode['code']));
        $this->assertSame(true, !empty($episode['release']));
    }

    public function testEpisodeNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/episodes/S000Ep35352CXX');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testEpisodeS01E03IsOk()
    {
        $client = static::createClient();
        $client->request('GET', '/episodes/S01E03');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('ok', $responseData['status']);

        $episode = $responseData['data'];
        $this->assertSame(3, $episode['id']);
        $this->assertSame("Anatomy Park", $episode['name']);
        $this->assertSame("S01E03", $episode['code']);
        $this->assertSame("December 16, 2013", $episode['release']);

        $this->assertSame('Test text with sentiment -0.2', $episode['reviews'][0]['text']);
        $this->assertSame(-0.2, $episode['reviews'][0]['sentiment']);
        $this->assertSame('Test text with sentiment -0.9', $episode['reviews'][1]['text']);
        $this->assertSame(-0.9, $episode['reviews'][1]['sentiment']);
    }

    public function testEpisodeS01E03PostEmptyReview()
    {
        $client = static::createClient();
        $client->request('POST', '/episodes/S01E03/review', ['text' => '']);

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('error', $responseData['status']);

        $client->request('GET', '/episodes/S01E03');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $episode = $responseData['data'];

        $this->assertSame(2, count($episode['reviews']));
    }

    public function testEpisodeS01E03PostPositiveReview()
    {
        $client = static::createClient();
        $client->request('POST', '/episodes/S01E03/review', ['text' => 'This is better than previous one.']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('ok', $responseData['status']);

        $client->request('GET', '/episodes/S01E03');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $episode = $responseData['data'];

        $this->assertSame('This is better than previous one.', $episode['reviews'][0]['text']);
        $this->assertSame(0.4404, $episode['reviews'][0]['sentiment']);
        $this->assertSame('Test text with sentiment -0.2', $episode['reviews'][1]['text']);
        $this->assertSame(-0.2, $episode['reviews'][1]['sentiment']);
        $this->assertSame('Test text with sentiment -0.9', $episode['reviews'][2]['text']);
        $this->assertSame(-0.9, $episode['reviews'][2]['sentiment']);
    }

    public function testEpisodeS01E03PostNegativeReview()
    {
        $client = static::createClient();
        $client->request('POST', '/episodes/S01E03/review', ['text' => 'Worst episode in the history of the series.']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('ok', $responseData['status']);

        $client->request('GET', '/episodes/S01E03');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $episode = $responseData['data'];

        $this->assertSame('Test text with sentiment -0.2', $episode['reviews'][0]['text']);
        $this->assertSame(-0.2, $episode['reviews'][0]['sentiment']);
        $this->assertSame('Worst episode in the history of the series.', $episode['reviews'][1]['text']);
        $this->assertSame(-0.6249, $episode['reviews'][1]['sentiment']);
        $this->assertSame('Test text with sentiment -0.9', $episode['reviews'][2]['text']);
        $this->assertSame(-0.9, $episode['reviews'][2]['sentiment']);
    }

    public function testEpisodeS01E03Post2PositiveReviews()
    {
        $client = static::createClient();
        $client->request('POST', '/episodes/S01E03/review', ['text' => 'This is better than previous one.']);
        $client->request('POST', '/episodes/S01E03/review', ['text' => 'Just one more episode.']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('ok', $responseData['status']);

        $client->request('GET', '/episodes/S01E03');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $episode = $responseData['data'];

        $this->assertSame('This is better than previous one.', $episode['reviews'][0]['text']);
        $this->assertSame(0.4404, $episode['reviews'][0]['sentiment']);
        $this->assertSame('Just one more episode.', $episode['reviews'][1]['text']);
        $this->assertSame(0, $episode['reviews'][1]['sentiment']);
        $this->assertSame('Test text with sentiment -0.2', $episode['reviews'][2]['text']);
        $this->assertSame(-0.2, $episode['reviews'][2]['sentiment']);
    }




}